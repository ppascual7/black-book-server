const router = require('express').Router();
const userController = require('./../controllers/user')
const auth = require('./../auth')

// Register
router.post('/', (req, res, next) => {

    if(req.body.password !== req.body.confirmPassword) return res.send(false);

    userController.register(req.body).then( result => res.send(result))
})

// Login
router.post('/login', (req, res, next) => {
    
    userController.login(req.body).then( result => res.send(result))
})

// Activity solution:
/*
router.get('/details', (req, res, next) => {

    let decodedToken = auth.checkToken(req)
    
    if (decodedToken === "Unauthenticated") res.send("Unathenticated")

    else userController.getDetails(decodedToken.id).then( result => res.send(result)).catch()
})
*/
//

//Discussion solution
router.get('/details', auth.verify, (req, res) => {

    userController.getDetails(req.decodedToken.id).then(result => res.send(result))

})

router.post('/enroll', auth.verify, (req, res) => {

    userController.enroll(req.body, req.decodedToken.id).then(result => res.send(result))
})

router.get('/:userId', auth.verify, (req, res) => {
    userController.getUser(req.params.userId).then(result => res.send(result))
})

router.get('/', auth.verify, (req, res) => {
    userController.getAll().then(result => res.send(result))
})

router.delete('/:userId', auth.verify, (req, res) => {
    userController.deleteUser(req.params.userId).then( user => res.send(user))
})

router.put('/', auth.verify, (req, res) => {
    console.log("put")
    userController.update(req.body).then( result => res.send(result))
})

module.exports = router