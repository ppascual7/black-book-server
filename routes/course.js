const router = require('express').Router();
const courseController = require('./../controllers/course')
const auth = require('./../auth')

// create course
router.post('/', auth.verify, (req, res, next) => {
    courseController.addCourse(req.body).then( course => res.send(course))
})

// get all courses

router.get('/', (req, res, next) => {
    courseController.getAll(req.query).then( course => res.send(course))
})

// get specific course
router.get('/:courseId', (req, res, next) => {
    courseController.get(req.params.courseId).then( course => res.send(course))
})

// delete specific course
router.delete('/:courseId', auth.verify,(req, res) => {
    courseController.archive(req.params.courseId).then( course => res.send(course))
})

// update course
router.put('/', auth.verify, (req, res) => {
    courseController.update(req.body).then( result => res.send(result))
})

module.exports = router;