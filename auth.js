const jwt = require('jsonwebtoken')

module.exports.createAccessToken = (registeredUser) => {
    return jwt.sign({ 
        id: registeredUser._id,
        email: registeredUser.email,
        isAdmin: registeredUser.isAdmin
     }, process.env.SECRET_KEY);
}

/* Activity solution 
module.exports.checkToken = (req) => {

    if(!req.headers.authorization) return ("Unauthenticated")
    
    return decodedToken = jwt.verify(req.headers.authorization.split(" ")[1], process.env.SECRET_KEY)
}
*/

//Discussion solution
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

    if( typeof token !== 'undefined'){
        token = token.split(" ")[1]

        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if (!err) req.decodedToken = decoded
            
            return err ? res.send({ auth: "failed" }) : next()
        })

    } else {
        return res.send({ auth: "failed" })
    }
}