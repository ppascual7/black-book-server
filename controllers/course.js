const Course = require('./../models/Course')

module.exports.addCourse = (reqBody) => {
    let course = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })

    return course.save()
    .then((course) => true)
    .catch(err => err);
}

module.exports.getAll = ({isActive}) => {

   if (isActive === undefined)
        return Course.find()
        .then( courses => courses )
        .catch( err => err )

   return Course.find({isActive})
   .then( courses => courses )
   .catch(err => err);

}

module.exports.get = (reqParamsCourseId) => {

    return Course.findById(reqParamsCourseId)
    .then( courses => courses )
    .catch( (err) => err);
 
 }

module.exports.archive = (reqParamsCourseId) => {
    return Course.findByIdAndUpdate(reqParamsCourseId, {isActive: false})
    .then( () => true)
    .catch( (err) => err );
}

module.exports.update = (reqBody) => {

    console.log(reqBody)

    return Course.findByIdAndUpdate(reqBody._id, {$set: reqBody}, {new: true})
    .then( () => true)
    .catch( () => false);
}

