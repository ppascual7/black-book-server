const User = require('./../models/Users')
const Course = require('./../models/Course')
const bcrypt = require('bcrypt');
const auth = require('./../auth.js')

module.exports.register = (reqBody) => {

    if (reqBody.password !== reqBody.confirmPassword) return 

    let user = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 4)
    })

    console.log(reqBody)

    return user.save()
    .then( () => true )
    .catch(() => false );
}

module.exports.login = (reqBody) => {

    //Destructured
    let { email, password } = reqBody;

    return User.findOne( {email} )
    .then ( registeredUser => {
        if ( !registeredUser ) return false;

        let isPasswordMatched = bcrypt.compareSync(password, registeredUser.password);

        if ( !isPasswordMatched ) return false;

        let accessToken = auth.createAccessToken(registeredUser)

        return {
            // Generate token:
            accessToken: accessToken
        }

    })
}

/*Activity Solution
module.exports.getDetails = (IdFromToken) => {

    return User.findById(IdFromToken, {password: 0})
    .then ( result => result )
    .catch ( err => err )
 
}
*/

//Discussion Solution
module.exports.getDetails = (decodedId) => {

    return User.findById(decodedId, { password: 0 })
    .then( user => {
        return user
    })
}

module.exports.getUser = (userId) => {

    return User.findById(userId)
    .then ( user => {
        return user
    })
}

module.exports.getAll = () => {

    return User.find()
    .then ( user => {
        return user
    })
}

module.exports.deleteUser = (userId) => {

    return User.findByIdAndDelete(userId)
    .then ( user => {
        if (!user) return false
        return true
    })
}

module.exports.enroll = (reqBody, decodedId) => {

    return Course.findById(reqBody._id)

    .then ( courses =>  {
        if ( !courses ) return "Invalid courseId"

        //const enrolledDate = new Date

        const enrollmentDetails = {
            courseId: reqBody._id,
            //enrolledOn: enrolledDate,
            //status: "enrolled"
        }

        return User.findByIdAndUpdate(decodedId, {
            $push:{
                enrollments: enrollmentDetails
            }}, {new: true})    

        .then( (enrolledCourses) => {

            const enrolleesDetails = {
                //enrolledOn: new Date(),
                userId: decodedId,
            }

            return Course.findByIdAndUpdate(reqBody._id, {
                $push: {
                    enrollees: enrolleesDetails
                
            }}, {new: true})
            .then( (enrollees) => true )
            .catch( () => false)

        })
        .catch( () => false);

    })
    .catch ( () => false )

}

module.exports.update = (reqBody) => {

    console.log(reqBody)

    return User.findByIdAndUpdate(reqBody._id, {$set: reqBody}, {new: true})
    .then( () => true)
    .catch( () => false);
}

