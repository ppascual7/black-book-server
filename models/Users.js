const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        unique: [true, "Email exists"]
    },
    password: {
        type: String,
        required: true,
        minlen: 10
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"],
        minlen: 11
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, 'Course ID is required']
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: 'Enrolled'
            }
        }
    ]
})

module.exports = mongoose.model('Users', userSchema)