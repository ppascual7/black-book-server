const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dotenv = require('dotenv');
const cors = require('cors');

const coursesRoutes = require('./routes/course.js')
const userRoutes = require('./routes/user.js')

const port = process.env.PORT || 4000;

dotenv.config({path: './config/.env'});

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true});

mongoose.connection.once('open', () => {
    console.log("Connected to Database");
})
mongoose.connection.on('error', () => {
    console.log("Not connected to Database");
})

//Middleware for cors
app.use(cors());

//Middleware for express json
app.use(express.json());



app.use('/api/courses', coursesRoutes)
app.use('/api/users', userRoutes)

app.listen( port, () => {
    console.log(`App is listening on port ${port}`);
})